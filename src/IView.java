/*
 * Paolo Liberini - paololiberini@gmail.com
 * Pong: progetto personale per il corso di programmazione ad oggetti (Java)
 * */

public interface IView {

    void update();
}
